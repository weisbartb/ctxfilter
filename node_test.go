package filter_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"

	"filter"
)

type testMarshalInterface interface {
	Test()
}

type testMarshalFirstLevel struct {
	AString         string  `json:"aString"`
	ABool           bool    `json:"aBool"`
	AnInt           int     `json:"anInt"`
	AnUint          uint    `json:"anUint"`
	AFloat          float64 `json:"aFloat"`
	ShouldBeIgnored string  `json:"-"`
}
type testStructWithMap struct {
	Items map[int]int `json:"items"`
}
type testNestedMarshal struct {
	Item1 testMarshalFirstLevel `json:"item1"`
}

type testNestedPointerMarshal struct {
	Item1 *testMarshalFirstLevel `json:"item1"`
}

func (t testNestedPointerMarshal) Test() {}

type testListOfPointers struct {
	List []*testMarshalFirstLevel `json:"list"`
}

type testListOfInterfaces struct {
	List []testMarshalInterface `json:"list"`
}

type testByteList struct {
	ShouldBeBase64  []byte          `json:"shouldBeBase64"`
	ShouldBeBase642 byteStringAlias `json:"shouldBeBase642"`
}
type embeddedLevelOneTest struct {
	CoreString string `json:"coreString"`
	embeddedLevelTwoTest
}
type embeddedLevelTwoTest struct {
	EmbeddedString string `json:"embeddedString"`
}

type recursiveType struct {
	Id       int             `json:"id"`
	Children []recursiveType `json:"children"`
}

type customMarshalType struct {
	item embeddedLevelOneTest
}

func (m customMarshalType) EncodeJSON(e filter.Encoder) ([]byte, error) {
	return filter.MarshalToJSON(m.item, e)
}

func (m customMarshalType) IsEmpty() bool {
	return len(m.item.CoreString) == 0
}

var ctx = filter.NewFilterContext(context.Background())

type byteStringAlias []byte

var compare = func(t *testing.T, resp, expectedResponse string) {
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value saw - %s [len=%v], expected %s [len=%v]", []byte(resp), len(resp), []byte(expectedResponse), len(expectedResponse))
		t.FailNow()
	}
}

func TestMarshalJSON(t *testing.T) {
	item := testMarshalFirstLevel{
		ABool:           true,
		AString:         "Test",
		AnInt:           -2,
		AnUint:          4,
		AFloat:          24.234,
		ShouldBeIgnored: "Don't export me",
	}

	resp, err := filter.MarshalToJSON(item, filter.NewEncoder(ctx, filter.Chain{}))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	var expectedResponse = `{"aString":"Test","aBool":true,"anInt":-2,"anUint":4,"aFloat":24.234}`
	compare(t, string(resp), expectedResponse)
	item2 := testNestedMarshal{
		Item1: item,
	}
	resp, err = filter.MarshalToJSON(item2, filter.NewEncoder(ctx, filter.Chain{}))
	expectedResponse = `{"item1":{"aString":"Test","aBool":true,"anInt":-2,"anUint":4,"aFloat":24.234}}`
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value saw - %v, expected %v", string(resp), expectedResponse)
		t.FailNow()
	}
	ptrItem := testNestedPointerMarshal{
		Item1: &item,
	}
	item.AnInt = 5
	resp, err = filter.MarshalToJSON(ptrItem, filter.NewEncoder(ctx, filter.Chain{}))
	expectedResponse = `{"item1":{"aString":"Test","aBool":true,"anInt":5,"anUint":4,"aFloat":24.234}}`
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value saw - %v, expected %v", string(resp), expectedResponse)
		t.FailNow()
	}
	var tmpInterface = testMarshalInterface(ptrItem)
	item.AnInt = -4
	resp, err = filter.MarshalToJSON(tmpInterface, filter.NewEncoder(ctx, filter.Chain{}))
	expectedResponse = `{"item1":{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234}}`
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value")
		t.FailNow()
	}
	var tmpListOfInterfaces = testListOfInterfaces{
		List: []testMarshalInterface{
			tmpInterface,
			tmpInterface,
			tmpInterface,
			tmpInterface,
		},
	}
	resp, err = filter.MarshalToJSON(tmpListOfInterfaces, filter.NewEncoder(ctx, filter.Chain{}))
	// fmt.Printf("%s", string(resp))
	expectedResponse = `{"list":[{"item1":{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234}},{"item1":{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234}},{"item1":{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234}},{"item1":{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234}}]}`
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value saw - %v, expected %v", string(resp), expectedResponse)
		t.FailNow()
	}
	var tmpMapTest = testStructWithMap{
		Items: map[int]int{
			1: 2,
			3: 5,
			8: 13,
		},
	}
	resp, err = filter.MarshalToJSON(tmpMapTest, filter.NewEncoder(ctx, filter.Chain{}))
	expectedResponse = `{"items":{"1":2,"3":5,"8":13}}`
	var tmpMapTest2 = testStructWithMap{Items: map[int]int{}}
	err = json.Unmarshal([]byte(expectedResponse), &tmpMapTest2)
	if err != nil {
		t.Errorf("Could not unmarshal expected response - %v", err)
		t.FailNow()
	}
	for k, v := range tmpMapTest2.Items {
		if tmpMapTest.Items[k] != v {
			t.Errorf("Map test should be indential and was not.")
			t.FailNow()
		}
	}
	byteList := testByteList{
		ShouldBeBase64:  []byte{'a', 'b', 'c'},
		ShouldBeBase642: []byte{'a', 'b', 'c'},
	}
	resp, err = filter.MarshalToJSON(byteList, filter.NewEncoder(ctx, filter.Chain{}))
	expectedResponse = `{"shouldBeBase64":"YWJj","shouldBeBase642":"YWJj"}`
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value saw - %v, expected %v", string(resp), expectedResponse)
		t.FailNow()
	}
	listOfItems := testListOfPointers{
		List: []*testMarshalFirstLevel{
			&item,
			&item,
			&item,
		},
	}
	resp, err = filter.MarshalToJSON(listOfItems, filter.NewEncoder(ctx, filter.Chain{}))
	expectedResponse = `{"list":[{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234},{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234},{"aString":"Test","aBool":true,"anInt":-4,"anUint":4,"aFloat":24.234}]}`
	if !strings.EqualFold(string(resp), expectedResponse) {
		t.Errorf("Failed to eval expected string value saw - %v, expected %v", string(resp), expectedResponse)
		t.FailNow()
	}
	// fmt.Printf("%s", string(resp))
}

func BenchmarkBytesVsStringsBuffer(b *testing.B) {
	benchmarks := []struct {
		name string
	}{
		{name: "strings-pre-alloc-as-string"},
		{name: "bytes-pre-alloc-as-string"},
		{name: "strings-as-string"},
		{name: "bytes-as-string"},
		{name: "strings-pre-alloc-as-bytes"},
		{name: "bytes-pre-alloc-as-bytes"},
		{name: "strings-as-bytes"},
		{name: "bytes-as-bytes"},
	}
	for _, bm := range benchmarks {
		b.SetParallelism(1)
		b.Run(bm.name, func(b *testing.B) {
			b.StartTimer()
			for i := 0; i < b.N; i++ {
				switch bm.name {
				case "strings-as-string":
					var tmp string

					buf := strings.Builder{}
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = buf.String()

					_ = tmp
				case "bytes-as-string":
					var tmp string
					buf := bytes.Buffer{}
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = buf.String()

					_ = tmp
				case "strings-pre-alloc-as-string":
					var tmp string
					buf := strings.Builder{}
					buf.Grow(47)
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = buf.String()

					_ = tmp
				case "bytes-pre-alloc-as-string":
					var tmp string
					buf := bytes.Buffer{}
					buf.Grow(47)
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = buf.String()

					_ = tmp
				case "strings-as-bytes":
					var tmp []byte
					buf := strings.Builder{}
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = []byte(buf.String())

					_ = tmp
				case "bytes-as-bytes":
					var tmp []byte
					buf := bytes.Buffer{}
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = buf.Bytes()

					_ = tmp
				case "strings-pre-alloc-as-bytes":
					var tmp []byte
					buf := strings.Builder{}
					buf.Grow(47)
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = []byte(buf.String())

					_ = tmp
				case "bytes-pre-alloc-as-bytes":
					var tmp []byte
					buf := bytes.Buffer{}
					buf.Grow(47)
					buf.WriteString(`{"amount":`)
					buf.WriteString(strconv.Itoa(2000))
					buf.WriteString(`,"isoCode":"`)
					buf.WriteString("USD")
					buf.WriteString(`"}`)
					tmp = buf.Bytes()

					_ = tmp
				}
			}
			b.StopTimer()
		})
	}
}

func Benchmark(b *testing.B) {

	benchmarks := []struct {
		name string
	}{
		{name: "Filter"},
		{name: "stdLib"},
	}
	var tmpMap = testStructWithMap{
		Items: func() map[int]int {
			var tmp = make(map[int]int)
			i := 1
			prev := 1
			for i < 1000000000 {
				prev = i
				tmp[i] = prev + i
				i = prev + i
			}
			return tmp
		}(),
	}
	_, err := filter.MarshalToJSON(tmpMap, filter.NewEncoder(ctx, filter.Chain{}))
	if err != nil {
		fmt.Printf("Failure to marshal tmpMap - %v", err)
		os.Exit(1)
	}
	var allocation []byte
	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {

				switch bm.name {
				case "Filter":
					allocation, err = filter.MarshalToJSON(tmpMap, filter.NewEncoder(ctx, filter.Chain{}))
				case "stdLib":
					_, _ = json.Marshal(tmpMap)
				}
			}
		})
	}
	if len(allocation) > 0 {

	}
}

func TestMarshalEmbedded(t *testing.T) {
	i := embeddedLevelOneTest{
		embeddedLevelTwoTest: embeddedLevelTwoTest{
			EmbeddedString: "Embedded",
		},
		CoreString: "Core",
	}
	data, _ := filter.MarshalToJSON(i, filter.NewEncoder(ctx, filter.Chain{}))
	// expect it to be set to this
	var expectedResponse = `{"coreString":"Core","embeddedString":"Embedded"}`
	compare(t, string(data), expectedResponse)

}
func TestMarshalRecursive(t *testing.T) {
	i := recursiveType{
		Id: 1,
		Children: []recursiveType{
			{
				Id: 2,
				Children: []recursiveType{
					{Id: 3},
				},
			},
		},
	}
	data, _ := filter.MarshalToJSON(i, filter.NewEncoder(ctx, filter.Chain{}))
	compare(t, string(data), `{"id":1,"children":[{"id":2,"children":[{"id":3,"children":[]}]}]}`)
}

func TestMarshalCustom(t *testing.T) {
	i := customMarshalType{
		item: embeddedLevelOneTest{
			CoreString: "core",
			embeddedLevelTwoTest: embeddedLevelTwoTest{
				EmbeddedString: "embed",
			},
		},
	}
	data, _ := filter.MarshalToJSON(i, filter.NewEncoder(ctx, filter.Chain{}))
	compare(t, string(data), `{"coreString":"core","embeddedString":"embed"}`)
}
