package filter

import (
	"bytes"
	"encoding"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"reflect"
	"strconv"
	"unicode/utf8"
)

var UseDefaultEncode = errors.New("use default encoding method for json")

var (
	filterMarshalerType = reflect.TypeOf(new(IContentJSONMarshal)).Elem()
	jsonMarshalerType   = reflect.TypeOf(new(json.Marshaler)).Elem()
	textMarshalerType   = reflect.TypeOf(new(encoding.TextMarshaler)).Elem()
)

// Jump table of what items can be reflected against for map keys
var allowedMapKeys = map[reflect.Kind]bool{
	reflect.Int:    true,
	reflect.Int8:   true,
	reflect.Int16:  true,
	reflect.Int32:  true,
	reflect.Int64:  true,
	reflect.Uint:   true,
	reflect.Uint8:  true,
	reflect.Uint16: true,
	reflect.Uint32: true,
	reflect.Uint64: true,
	reflect.String: true,
}

// An encoder wrapper to clean up method signatures
type Encoder struct {
	ctx *Context
	// This exists so it can be passed through to the JSON encoder when new nodes are spawned
	filters           Chain
	suppressionActive bool
	suppressedFields  map[string]bool
}

func (e Encoder) CloneEncoder(chain Chain) Encoder {
	e.filters = chain
	return e
}
func (e Encoder) GetFilterChain() Chain {
	return e.filters
}

func (e Encoder) SetFilters(chain Chain) Encoder {
	e.filters = chain
	return e
}
func (e Encoder) SetCTX(ctx *Context) Encoder {
	e.ctx = ctx
	return e
}
func (e Encoder) SuppressField(i interface{}) Encoder {
	if e.suppressedFields == nil {
		e.suppressedFields = make(map[string]bool)
		e.suppressionActive = true
	}
	typ := reflect.TypeOf(i)
	e.suppressedFields[typ.PkgPath()+"/"+typ.Name()] = true
	return e
}
func (e Encoder) ClearSuppressedFields() Encoder {
	if e.suppressedFields != nil {
		for k := range e.suppressedFields {
			delete(e.suppressedFields, k)
		}
	}
	return e
}
func (e Encoder) IsSuppressed(fieldName string) bool {
	if val, ok := e.suppressedFields[fieldName]; ok {
		return val
	}
	return false
}
func (e Encoder) GetCTX() *Context {
	return e.ctx
}

func NewEncoder(ctx *Context, filters Chain) Encoder {
	return Encoder{ctx: ctx, filters: filters}
}

// Encoder helper methods
// Encode a boolean item
func (e Encoder) encodeBool(value reflect.Value, response *bytes.Buffer) {
	if value.Bool() {
		response.WriteString("true")
	} else {
		response.WriteString("false")
	}
}

// Encode a float value
func (e Encoder) encodeFloat(value reflect.Value, response *bytes.Buffer) {
	b := make([]byte, 0)
	abs := math.Abs(value.Float())
	_fmt := byte('f')
	// Note: Must use float32 comparisons for underlying float32 value to get precise cutoffs right.
	if abs != 0 {
		if abs < 1e-6 || abs >= 1e21 {
			_fmt = 'e'
		}
	}
	b = strconv.AppendFloat(b, value.Float(), _fmt, -1, 64)
	if _fmt == 'e' {
		// clean up e-09 to e-9
		n := len(b)
		if n >= 4 && b[n-4] == 'e' && b[n-3] == '-' && b[n-2] == '0' {
			b[n-2] = b[n-1]
			b = b[:n-1]
		}
	}
	response.Write(b)
}

// See if a type supports a custom marshalling method
func (e Encoder) supportsCustomMarshal(t reflect.Type) bool {
	return t.Implements(filterMarshalerType) || t.Implements(jsonMarshalerType) || t.Implements(textMarshalerType)
}

// Handle custom marshalling
func (e Encoder) handleCustomMarshal(value reflect.Value) ([]byte, error) {

	kind := value.Type().Kind()
	var resolutionsRemaining = 5
	// Need to resolve pointers as their named value doesn't exist in their struct info
	if kind == reflect.Ptr {
		for value.Kind() == reflect.Ptr && resolutionsRemaining > 0 {
			if value.IsNil() {
				return []byte("null"), nil
			}
			value = value.Elem()
			resolutionsRemaining--
			break
		}
		if resolutionsRemaining == 0 && value.Kind() == reflect.Ptr {
			return nil, fmt.Errorf("could not finish decoding %v ", value.Kind().String())
		}
	}
	if !value.CanInterface() {
		return nil, nil
	}
	if contentInterface, ok := value.Interface().(IContentJSONMarshal); ok {
		// See if the value is empty, if so return null and let the higher level encoder sort out what to do about it
		if value.Kind() == reflect.Ptr && value.IsNil() {
			return []byte("null"), nil
		}
		if contentInterface.IsEmpty() {
			return []byte("null"), nil
		}
		if data, err := contentInterface.EncodeJSON(e); err == nil {
			return data, nil
		} else {
			return nil, err
		}
	}
	// Check if it can zero

	if zeroInterface, ok := value.Interface().(CanZero); ok {
		if zeroInterface.IsZero() {
			if defaultInterface, ok := value.Interface().(DefaultValue); ok {
				return defaultInterface.Default(), nil
			} else {
				return []byte("null"), nil
			}
		}
	}
	if jm, ok := value.Interface().(json.Marshaler); ok {
		if data, err := jm.MarshalJSON(); err == nil {
			return data, nil
		} else {
			return nil, err
		}
	}
	if tm, ok := value.Interface().(encoding.TextMarshaler); ok {
		if data, err := tm.MarshalText(); err == nil {
			return data, nil
		} else {
			return nil, err
		}
	}

	return nil, nil
}

// Handle encoding of structs
func (e Encoder) encodeStruct(value reflect.Value, response *bytes.Buffer) error {
	var data []byte
	var err error
	// Handle any struct that can comply with the interface contract for a content filter
	if data, err = e.handleCustomMarshal(value); err != nil {
		// Allow for content to bypass through a custom encoder, useful for handling IsEmpty without multiple interface checks
		if err != UseDefaultEncode {
			return err
		}
	}
	if data != nil {
		response.Write(data)
		return nil
	}
	n := decodeType(value)
	if b, marshalErr := n.marshalStruct(value, e); marshalErr != nil {
		return marshalErr
	} else {
		response.Write(b)
	}
	return nil
}

// Encode a map
func (e Encoder) encodeMap(value reflect.Value, response *bytes.Buffer) error {
	if _, ok := allowedMapKeys[value.Type().Key().Kind()]; !ok {
		return fmt.Errorf("unsupported map type containing a key of %v", value.Type().Key().Kind())
	}
	keys := value.MapKeys()
	response.WriteByte('{')
	// This can be optimized further by using a look-ahead on the first key to rally the type since go doesn't
	// 	support mixed types. However it will substantially bloat the method as you will need to write a for loop
	// 	for every case rather than a single for loop. This is a case where the code size trumps performance.
	// 	Would be fun to write a pre-compiler that can unroll this though...
	for i, v := range keys {
		if i > 0 {
			response.WriteByte(',')
		}
		response.WriteByte('"')
		if tm, ok := v.Interface().(encoding.TextMarshaler); ok {
			if buf, err := tm.MarshalText(); err != nil {
				return err
			} else {
				response.Write(buf)
			}
		} else {

			switch v.Kind() {
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				response.WriteString(strconv.FormatInt(v.Int(), 10))
			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
				response.WriteString(strconv.FormatUint(v.Uint(), 10))
			case reflect.String:
				// UTF-8 Encoding
				e.encodeString(v.String(), response)
			default:
				return fmt.Errorf("saw a type of %v as a map key which is not supported", v.Type().Name())
			}

		}
		response.Write([]byte{'"', ':'})
		if b, err := e.encodeValue(value.MapIndex(v)); err != nil {
			return err
		} else {
			response.Write(b)
		}
	}
	response.WriteByte('}')
	return nil
}

func (e Encoder) encodeString(s string, buffer *bytes.Buffer) {
	const hex = "0123456789abcdef"
	start := 0
	for i := 0; i < len(s); {

		if b := s[i]; b < utf8.RuneSelf {
			if htmlSafeSet[b] {
				i++
				continue
			}
			if start < i {
				buffer.WriteString(s[start:i])
			}
			switch b {
			case '\\', '"':
				buffer.WriteByte('\\')
				buffer.WriteByte(b)
			case '\n':
				buffer.WriteByte('\\')
				buffer.WriteByte('n')
			case '\r':
				buffer.WriteByte('\\')
				buffer.WriteByte('r')
			case '\t':
				buffer.WriteByte('\\')
				buffer.WriteByte('t')
			default:
				// This encodes bytes < 0x20 except for \t, \n and \r.
				// If escapeHTML is set, it also escapes <, >, and &
				// because they can lead to security holes when
				// user-controlled strings are rendered into JSON
				// and served to some browsers.
				buffer.WriteString(`\u00`)
				buffer.WriteByte(hex[b>>4])
				buffer.WriteByte(hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRuneInString(s[i:])
		if c == utf8.RuneError && size == 1 {
			if start < i {
				buffer.WriteString(s[start:i])
			}
			buffer.WriteString(`\ufffd`)
			i += size
			start = i
			continue
		}
		// U+2028 is LINE SEPARATOR.
		// U+2029 is PARAGRAPH SEPARATOR.
		// They are both technically valid characters in JSON strings,
		// but don't work in JSONP, which has to be evaluated as JavaScript,
		// and can lead to security holes there. It is valid JSON to
		// escape them, so we do so unconditionally.
		// See http://timelessrepo.com/json-isnt-a-javascript-subset for discussion.
		if c == '\u2028' || c == '\u2029' {
			if start < i {
				buffer.WriteString(s[start:i])
			}
			buffer.WriteString(`\u202`)
			buffer.WriteByte(hex[c&0xF])
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		buffer.WriteString(s[start:])
	}
}

// Encode a slice or an array
func (e Encoder) encodeSlice(value reflect.Value, response *bytes.Buffer) error {
	if value.Len() == 0 {
		response.WriteString(`[]`)
		return nil
	}
	if value.Index(0).Kind() == reflect.Uint8 {
		p := reflect.PtrTo(value.Type())
		if !p.Implements(jsonMarshalerType) && !p.Implements(textMarshalerType) {
			if value.IsNil() {
				response.WriteString("null")
			} else {
				response.WriteByte('"')
				tmp := value.Bytes()
				if len(tmp) < 1024 {
					dst := make([]byte, base64.StdEncoding.EncodedLen(len(tmp)))
					base64.StdEncoding.Encode(dst, tmp)
					response.Write(dst)
				} else {
					b64enc := base64.NewEncoder(base64.StdEncoding, response)
					_, _ = b64enc.Write(tmp)
					_ = b64enc.Close()
				}
				response.WriteByte('"')
			}
		}
	} else {
		response.WriteByte('[')
		n := value.Len()
		for i := 0; i < n; i++ {
			if i > 0 {
				response.WriteByte(',')
			}
			if b, err := e.encodeValue(value.Index(i)); err != nil {
				return err
			} else {
				response.Write(b)
			}
		}
		response.WriteByte(']')
	}
	return nil
}

// Encode a ptr
// Ptrs are a little weird since you have to make calls to remove the indirect reference to them and make them a hard value then encode
func (e Encoder) encodePtr(value reflect.Value, response *bytes.Buffer) error {
	if value.IsNil() {
		response.WriteString("null")
	} else {
		n := decodeType(value.Elem())
		if b, err := n.marshalNode(value.Elem(), e); err != nil {
			return err
		} else {
			response.Write(b)
		}
	}
	return nil
}

// higher level encoding method that will take a value and figure out what encoder to user
func (e Encoder) encodeValue(value reflect.Value) ([]byte, error) {
	response := bytes.Buffer{}
	if e.supportsCustomMarshal(value.Type()) {
		data, err := e.handleCustomMarshal(value)
		if err == nil {
			return data, nil
		} else {
			if err != UseDefaultEncode {
				return nil, err
			}
		}
	}
	if e.suppressionActive {
		if e.IsSuppressed(value.Type().PkgPath() + "/" + value.Type().Name()) {
			return OmitEntry, nil
		}
	}

	switch value.Kind() {
	case reflect.Bool:
		// Handle boolean value encoding
		e.encodeBool(value, &response)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		// Handle signed integer encoding
		response.WriteString(strconv.FormatInt(value.Int(), 10))
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		// Handle unsigned integer encoding
		response.WriteString(strconv.FormatUint(value.Uint(), 10))
	case reflect.Float32, reflect.Float64:
		// Handle float encoding
		e.encodeFloat(value, &response)
	case reflect.String:
		// Handle string encoding
		response.WriteByte('"')
		e.encodeString(value.String(), &response)
		response.WriteByte('"')
	case reflect.Struct:
		// Encode a struct/object
		if err := e.encodeStruct(value, &response); err != nil {
			return nil, err
		}
	case reflect.Map:
		// Encode a map
		if err := e.encodeMap(value, &response); err != nil {
			return nil, err
		}
	case reflect.Slice, reflect.Array:
		// Encode an array or slice, we can treat these largely as the same for the
		// purpose of encoding
		if err := e.encodeSlice(value, &response); err != nil {
			return nil, err
		}
	case reflect.Ptr, reflect.Interface:
		// Handle encoding pointers, we can treat these largely as the same for the
		// purpose of encoding
		if err := e.encodePtr(value, &response); err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("could not encode %v", value.Kind().String())
	}
	return response.Bytes(), nil
}
