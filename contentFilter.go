package filter

import (
	"reflect"
)

type IContentJSONMarshal interface {
	// Encoding method for the the object
	EncodeJSON(e Encoder) ([]byte, error)
	// Is the value empty?
	IsEmpty() bool
}
type CanZero interface {
	IsZero() bool
}
type DefaultValue interface {
	Default() []byte
}
type IContentFilter interface {
	ActivateOn() string
	IsAllowed(t reflect.Type) bool
	Apply(value []byte, request *Context, params []string) ([]byte, error)
}
