package filter

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"strings"
	"sync"
	"sync/atomic"
	"unsafe"

	"github.com/pkg/errors"
)

// Struct caching system to make reflection cheaper on nodes
var structCache = make(map[reflect.Type]*node)
var structMutex = &sync.RWMutex{}

// 4 null bytes will trigger an omit entry
var OmitEntry = []byte{0, 0, 0, 0}

var ValidEncoderRequired = errors.New("a valid encoder is required (ctx missing)")
var InvalidPrimitiveProvided = errors.New("invalid primitive type provided")

const (
	jsonTypeUnknown int = iota
	jsonTypeString
	jsonTypeArray
	jsonTypeObject
	jsonTypeNumber
	jsonTypeBool
)

// A reflection node
type node struct {
	// Children that are linked to this node (for optimization later)
	children []*node
	// The name of the key that will be encoded, this defaults to the field name in the struct unless the proper
	// 	json tag is present
	outputName string
	jsonType   int
	// Force the output of integers as a string, I will be adding a special encoding mode later on that detects if the
	// 	number is greater than 54 bits and will encode it as a string should that occur based on the client settings.
	// 	for non-javascript languages this will be useless.
	outputAsString bool
	// This will cause the field to strip if it is null,
	omitEmpty bool
	// Output to parent node
	outputToParent bool
	// Private variables (do not pick up)
	notExported bool
	tags        map[string]string
	// Do a custom marshal for this node
	customMarshal bool
}

// Create a new filter chain
// Not threadsafe
func NewFilterChain(id string) *Chain {
	filterMap := make(contentFilterList, 0)
	return &Chain{
		filterMap: filterMap,
		filterId:  id,
	}
}

type Chain struct {
	filterMap contentFilterList
	filterId  string
}

func (chain Chain) GetId() string {
	return chain.filterId
}

// Add a new filter to an existing chain
func (chain *Chain) Add(filter IContentFilter) *Chain {
	if chain.filterMap == nil {
		chain.filterMap = []IContentFilter{filter}
	} else {
		chain.filterMap = append(chain.filterMap, filter)
	}
	return chain
}

// Encode a value to a writer
func (chain Chain) Encode(v interface{}, ctx *Context, w io.Writer) (n int, err error) {
	var data []byte
	data, err = MarshalToJSON(v, NewEncoder(ctx, chain))
	if err != nil {
		return 0, err
	}
	return w.Write(data)
}

// A content filter map for determining if the field should have a filter applied to it
type contentFilterList []IContentFilter

// Apply all filters to a specified output stream
// Errors that come from filters must always be non-fatal, an errored filter will not have its mutation applied
// 	however as its getting the underlying byte slice it can still mutate that stream. Ensure that your filters
// 	are written in a way that will not cause un-intended mutations to the stream.
func (m contentFilterList) ApplyFilters(data []byte, n *node, ctx *Context) ([]byte, []error) {
	if m == nil {
		return data, nil
	}
	if n == nil {
		return data, nil
	}
	// No tags that can be hooked
	if n.tags == nil {
		return data, nil
	}
	// We need to use fixed positions for that array responses so each filter can have its own tracked error boundary
	// This is a bit wasteful as it will allocate the memory even though this will be nil most of the time
	// However this is only going to be 8 bytes per filter and will only be stack allocated
	var errorList = make([]error, len(m))
	hadError := false
	for k, filter := range m {
		if tag := filter.ActivateOn(); len(tag) > 0 {
			if tagParams, found := n.tags[tag]; found {
				if tmp, err := filter.Apply(data, ctx, strings.Split(tagParams, ",")); err != nil {
					hadError = true
					errorList[k] = err
				} else {
					data = tmp
				}
			}
		} else {
			if tmp, err := filter.Apply(data, ctx, nil); err != nil {
				hadError = true
				errorList[k] = err
			} else {
				data = tmp
			}

		}
	}
	if hadError {
		return data, errorList[:]
	}
	return data, nil
}

// Marshal an item into JSON format
//
// Arguments:
// 	i - the interface you are looking to marshal
// 	e - the encoder the user for encoding the platform to
//
// Returns:
// 	A byte array of the JSON
// 	Any error that occurred during the process
func MarshalToJSON(i interface{}, e Encoder) ([]byte, error) {
	if e.ctx == nil {
		return nil, ValidEncoderRequired
	}
	return decodeType(i).marshalNode(reflect.ValueOf(i), e)
}

// Decodes a type and places it into a cached item
//
// Arguments
// i - the interface that is being decoded
//
// Returns:
// 	A data node information
func decodeType(i interface{}) *node {
	var typeOf reflect.Type
	switch i.(type) {
	case reflect.Value:
		typeOf = i.(reflect.Value).Type()
	case reflect.Type:
		typeOf = i.(reflect.Type)
	default:
		typeOf = reflect.TypeOf(i)
	}
	structMutex.RLock()
	var ok bool
	var data *node
	if data, ok = structCache[typeOf]; !ok {
		structMutex.RUnlock()
		structMutex.Lock()
		// Recheck now that there is a lock
		if data, ok = structCache[typeOf]; !ok {
			// We set this to nil to ensure that it doesn't crash on a recursive struct
			structCache[typeOf] = &node{}
			structMutex.Unlock()
			resolvedNode := parseType(typeOf)
			atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(&data)), unsafe.Pointer(resolvedNode))
			structCache[typeOf] = data
		} else {
			structMutex.Unlock()
		}

	} else {
		structMutex.RUnlock()
	}
	return data
}

func (n *node) marshalNode(value reflect.Value, e Encoder) ([]byte, error) {
	return e.encodeValue(value)
}

// Marshal a struct node into its JSON output
//
// Arguments:
// 	value - the reflection value
// 	ctx - the context that was passed into the marshalling function
// 	encoder - the encoder used for marsrhalling
//
// Returns:
// 	a byte slice of the marshalled value
// 	any errors that occurred during marshalling
func (n *node) marshalStruct(value reflect.Value, e Encoder) ([]byte, error) {
	if n == nil {
		return []byte{}, nil
	}
	if n.customMarshal {
		return e.handleCustomMarshal(value)
	}
	buf := bytes.NewBufferString(`{`)
	if n.children != nil {
		for k, v := range n.children {
			if v.notExported && !v.outputToParent {
				continue
			}
			var target reflect.Value
			var resolutionsRemaining = 5
			// Pointer resolver
			for value.Kind() == reflect.Ptr && resolutionsRemaining > 0 {

				value = value.Elem()
				resolutionsRemaining--
			}
			if resolutionsRemaining == 0 && value.Kind() == reflect.Ptr {
				return nil, fmt.Errorf("could not finish decoding %v ", value.Kind().String())
			}
			target = value.Field(k)
			if data, err := e.encodeValue(target); err != nil {
				return nil, err
			} else {
				var errorList []error
				data, errorList = e.filters.filterMap.ApplyFilters(data, v, e.ctx)
				if errorList != nil && len(errorList) > 0 {
					return nil, errorList[0]
				}
				// Ensure there isn't a special filter drop command
				if !bytes.Equal(data, OmitEntry) {
					// Check for various legal states of "null" for omitEmpty to trigger
					if v.omitEmpty &&
						(
						// See if the data is empty
						len(data) == 0 ||
							// Check to see if the data is set to null
							bytes.Equal(data, []byte("null")) ||
							bytes.Equal(data, []byte("NULL")) ||
							(
							// Check to see if various forms of empty may exist
							len(data) == 2 && (bytes.Equal(data, []byte{'{', '}'}) ||
								bytes.Equal(data, []byte{'"', '"'}) ||
								bytes.Equal(data, []byte{'[', ']'})))) {
						continue
					}
					if k > 0 && buf.Len() > 1 {
						buf.WriteByte(',')
					}
					if v.outputToParent {
						// Need to trim byte range for { and }
						if data != nil {
							if data[0] == '{' {
								data = data[1 : len(data)-1]
							}
							buf.Write(data)
						}
					} else {
						buf.WriteByte('"')
						buf.WriteString(v.outputName)
						buf.Write([]byte{'"', ':'})
						if len(data) > 0 {
							if n.outputAsString && data[0] != '"' {
								buf.WriteByte('"')
								buf.Write(data)
								buf.WriteByte('"')
							} else {
								buf.Write(data)
							}
						} else {
							switch v.jsonType {
							case jsonTypeBool:
								buf.WriteString(`false`)
							case jsonTypeString:
								buf.WriteString(`""`)
							case jsonTypeArray:
								buf.WriteString(`[]`)
							case jsonTypeObject:
								buf.WriteString(`null`)
							case jsonTypeNumber:
								buf.WriteString(`0`)
							case jsonTypeUnknown:
								return nil, InvalidPrimitiveProvided
							default:
								buf.WriteString(`null`)
							}
						}
					}
				}
			}

		}
	}
	buf.WriteByte('}')
	return buf.Bytes(), nil
}

// Parse the type and build a node with tags being extracted
func parseType(t reflect.Type) *node {
	n := &node{}
	flattenItems := make([]*node, 0)
	if t.Kind() == reflect.Ptr {
		return parseType(t.Elem())
	}
	if t.Kind() == reflect.Struct {
		if canExportMarshal(t) {
			n.customMarshal = true
		}
		n.children = make([]*node, t.NumField())
		for i := 0; i < t.NumField(); i++ {
			child := &node{
				tags: parseTags(string(t.Field(i).Tag)),
			}
			jsonTags := strings.Split(child.tags["json"], ",")
			if len(jsonTags[0]) > 0 {
				for k, v := range jsonTags {
					switch k {
					case 0:
						child.outputName = v
						if child.outputName == "-" {
							child.notExported = true
							continue
						}
					default:
						switch v {
						case "omitempty":
							child.omitEmpty = true
						case "delift":
							child.outputToParent = true
						case "string":
							child.outputAsString = true
						}
					}

				}
			}

			if t.Field(i).Name[0] >= 'a' && t.Field(i).Name[0] <= 'z' {
				child.notExported = true
			}
			switch t.Field(i).Type.Kind() {
			case reflect.Struct:
				child.jsonType = jsonTypeObject
			case reflect.Bool:
				child.jsonType = jsonTypeBool
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Float32, reflect.Float64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				child.jsonType = jsonTypeNumber
			case reflect.Slice:
				child.jsonType = jsonTypeArray
			case reflect.String:
				child.jsonType = jsonTypeString
			default:
				child.jsonType = jsonTypeUnknown
			}
			if t.Field(i).Anonymous || !child.notExported || canExportMarshal(t.Field(i).Type) {

				var name = t.Field(i).Name
				if child.outputName == "" {
					child.outputName = name
				}

				tmp := decodeType(t.Field(i).Type)
				// Check to see if this struct is embedded into the other or not
				if t.Field(i).Type.Kind() == reflect.Struct && t.Field(i).Anonymous {
					child.outputToParent = true
				}
				if tmp != nil {
					child.children = tmp.children
				}
			}
			n.children[i] = child
		}
		n.children = append(n.children, flattenItems...)
	}
	return n
}

func canExportMarshal(typ reflect.Type) bool {
	return typ.Implements(filterMarshalerType) || typ.Implements(jsonMarshalerType)
}
