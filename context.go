package filter

import (
	"context"
	"time"
)

func NewFilterContext(ctx context.Context) *Context {
	return &Context{
		base: ctx,
	}
}

type Context struct {
	base context.Context
}

func (ctx Context) Deadline() (deadline time.Time, ok bool) {
	return ctx.base.Deadline()
}

func (ctx Context) Done() <-chan struct{} {
	return ctx.base.Done()
}

func (ctx Context) Err() error {
	return ctx.base.Err()
}

func (ctx Context) Value(key interface{}) interface{} {
	return ctx.base.Value(key)
}
