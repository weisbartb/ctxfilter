# Context-Aware JSON Encoder

### Why?

I needed a JSON encoder that could take in context, look at byte streams and rewrite them as needed.

### Performance
* Performance is OK, not great. This will allocate about 30% more memory and be about 33% slower than the standard encoder. A lot of this is due to the fact it creates buffer copies to allow the filters to manipulate data without potentially rewriting the encoding blob.

### Todo
* Rewrite bytes.buffer so I can have an easily resizable buffered array with good allocation properties. Filters make this hard
* Add more stuff to do this read me