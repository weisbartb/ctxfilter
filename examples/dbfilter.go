package examples

import (
	"reflect"

	"filter"
)

const dbFilterId = "db"

const (
	dbNormalized   = "normalized"
	dbDocument     = "document"
	dbOmit         = "omit"
	dbDenormalized = "denormalized"
)

type DatabaseFilter struct {
}

func (df DatabaseFilter) ActivateOn() string {
	return dbFilterId
}

// Allowed on all types
func (df DatabaseFilter) IsAllowed(t reflect.Type) bool {
	return true
}

func (df DatabaseFilter) Apply(value []byte, request *filter.Context, params []string) ([]byte, error) {
	// First param is the db column
	for _, param := range params[1:] {
		switch param {
		case dbDenormalized:
			return value, nil
		case dbNormalized, dbDocument, dbOmit:
			return filter.OmitEntry, nil
		}
	}
	return value, nil
}

var _filterChain = filter.NewFilterChain(dbFilterId).Add(DatabaseFilter{})
var DatabaseFilterChain = func() filter.Chain {
	return *_filterChain
}
